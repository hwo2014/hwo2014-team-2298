var _ = require('lodash');
var Piece = require('./piece.js');

function Track(track) {
  this.id = track.id;
  this.name = track.name;
  this.pieces = _.map(track.pieces, function(x) { return new Piece(x); });
  this.lanes = _.map(track.lanes, function(x) { return x; });
}

Track.prototype.pieceByIndex = function (index) {
  return this.pieces[index % this.pieces.length];
}

Track.prototype.pieceCount = function() {
  return this.pieces.length;
}

Track.prototype.trackLength = function (laneIndex) {
  var lane = _.first(_.filter(this.lanes, function(x) { return x.index === laneIndex; }));

  return _.reduce(this.pieces, function(sum, x) { return sum + x.length(lane); }, 0);
}

Track.prototype.lengthForLaneUntilNextSwitch = function (laneIndex, fromPieceIndex) {
  var lane = _.first(_.filter(this.lanes, function(x) { return x.index === laneIndex; }));
  var initialPiece = this.pieceByIndex(fromPieceIndex);
  var piecesToLookForward = this.pieceCount() + fromPieceIndex;

  var toPieceIndex = 0;
  for (var i = fromPieceIndex; i <= piecesToLookForward; i++) {
    if (this.pieceByIndex(i).canSwitchLanes())
    {
      toPieceIndex = i;
      break;
    }
  }

  var track = this;
  var length = _.reduce(
    _.map(
      _.range(fromPieceIndex, toPieceIndex),
      function (i) { return track.pieceByIndex(i); }
    ),
    function (sum, x) { return sum + x.length(lane); },
    0
  );

  return length;
};

Track.prototype.shortestLaneUntilNextSwitch = function (fromPieceIndex) {
  var track = this;
  var selectedLane;
  var minLength;
  _.each(this.lanes, function (x) {
    var laneLength = track.lengthForLaneUntilNextSwitch(x.index, fromPieceIndex);
    if (!minLength || laneLength < minLength) {
      minLength = laneLength;
      selectedLane = x.index;
    }
  });

  return selectedLane;
};

module.exports = Track;
