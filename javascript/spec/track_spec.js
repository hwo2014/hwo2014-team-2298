var should = require('should');
var Track = require('../track.js');
var Piece = require('../piece.js')

describe('Track', function() {
  var track;

  beforeEach(function () {
    var payload = {
      'id': 'keimola',
      'name': 'Keimola',
      'pieces': [
         {'length': 100}, {'length': 100}, {'length': 100}
      ],
      'lanes': [
        {
          'distanceFromCenter': -20,
          'index': 0
        },
        {
          'distanceFromCenter': 0,
          'index': 1
        }
      ]
   };

    track = new Track(payload);
  });

  it('knows its id', function() {
    track.id.should.be.exactly('keimola');
  });

  it('knows its name', function() {
    track.name.should.be.exactly('Keimola');
  });

  it('knows its track length', function() {
    track.trackLength(track.lanes[1]).should.be.exactly(300);
  });

  it('knows its piece count', function() {
    track.pieceCount().should.be.exactly(3);
  });

  it('construct its pieces', function() {
    track.pieces[0].should.be.an.instanceOf(Piece);
  });

  describe('Lane length', function () {
    beforeEach(function () {
      var payload = {
        'id': 'keimola',
        'name': 'Keimola',
        'pieces': [
           {'length': 100},
           {'length': 100, 'switch': true},
           {'length': 100},
           {'length': 100},
           {'radius': 100, 'angle': 45},
           {'length': 100, 'switch': true}
        ],
        'lanes': [
          {
            'distanceFromCenter': -20,
            'index': 0
          },
          {
            'distanceFromCenter': 0,
            'index': 1
          }
        ]
     };

      track = new Track(payload);
    });

    it('knows length until next switch', function () {
      var length0 = track.lengthForLaneUntilNextSwitch(0, 2);
      var length1 = track.lengthForLaneUntilNextSwitch(1, 2);

      length1.should.be.lessThan(length0);
    });

    it('knows shortestLane until next switch', function () {
      var lane = track.shortestLaneUntilNextSwitch(1);

      lane.should.be.exactly(1);
    });
  });
});