var should = require('should');
var Car = require("../car.js");

describe("Car", function() {
  var car;

  beforeEach(function () {
    var client = { on: function () {}, send: function () {}};
    var track = {};

    car = new Car('faster', 'red');
    car.moveOnTrack(client, track);
  });

  it('knows its name', function() {
    car.name.should.be.exactly('faster');
  });

  it('knows its color', function() {
    car.color.should.be.exactly('red');
  });

  it('knows its instant speed', function() {
    car.setPosition({
      "angle": 0.0,
      "piecePosition": {
        "pieceIndex": 0,
        "inPieceDistance": 10.0,
        "lane": {
          "startLaneIndex": 0,
          "endLaneIndex": 0
        },
        "lap": 0
      }
    }, 1);

    car.instantSpeed().should.be.exactly(10);
  });

  it('knows its instant speed when it didnt move', function() {
    car.instantSpeed().should.be.exactly(0);
  });

  it('knows its throttle', function() {
    car.throttle(1);

    car.currentThrottle.should.be.exactly(1);
  });

  it('increase its throttle', function() {
    car.throttle(0.5);

    car.increaseThrottle(0.5);

    car.currentThrottle.should.be.exactly(0.75);
  });

  it('decrease its throttle', function() {
    car.throttle(0.5);

    car.decreaseThrottle(0.5);

    car.currentThrottle.should.be.exactly(0.25);
  });
});
