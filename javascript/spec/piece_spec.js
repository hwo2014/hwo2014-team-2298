var should = require('should');
var Piece = require('../piece.js');

describe('Piece', function() {
  it('length of a straight piece', function() {
    var piece = new Piece({'length': 100});

    piece.length().should.be.exactly(100);
  });

  it.skip('length of an angle piece', function() {
    var piece = new Piece({'radius': 100, 'angle': 45});
    var lane = { distanceFromCenter: 0 };
    piece.length(lane).should.be.exactly(78.53981633974483);
  });

  it('length of an angle piece for a curved lane to the right', function() {
    var piece = new Piece({'radius': 90, 'angle': 45});
    var lane = { distanceFromCenter: -10 };
    piece.length(lane).should.be.exactly(78.53981633974483);
  });

  it('length of an angle piece for a curved lane to the left', function() {
    var piece = new Piece({'radius': 110, 'angle': -45});
    var lane = { distanceFromCenter: 10 };
    piece.length(lane).should.be.exactly(78.53981633974483);
  });

  it('knows if can switch lanes on this piece', function() {
    var piece = new Piece({'length': 100, 'switch': true});

    piece.canSwitchLanes().should.be.true;
  });
});
