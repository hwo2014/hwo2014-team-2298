var _ = require('lodash');
var Client = require('./client.js');
var Race = require('./race.js');
var Car = require('./car.js');
var Track = require('./track.js');
var Pilot = require('./pilot.js');
var fs = require('fs');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var raceTrack = process.env['TRACK'];
var raceCarCount = process.env['CARS'] || 1;
var racePassword = process.env['PASS'];
var outputFile = process.env['OUTPUT'] || '../race_log.txt';

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);
fs.writeFile(outputFile, 'Starting race');

var client = Client.connect(
  serverPort, serverHost,
  botKey, botName,
  raceTrack, raceCarCount, racePassword
);

client.on('error', function() {
  console.log('! error');
  setTimeout(function () { process.exit(0); }, 0);
});
client.on('close', function() {
  console.log('! socket closed');
  setTimeout(function () { process.exit(0); }, 0);
});

var ourCar;

client.on('yourCar', function (payload) {
  ourCar = new Car(payload.data.name, payload.data.color);
});
client.on('gameInit', function (payload) {
  var track = new Track(payload.data.race.track);
  ourCar.moveOnTrack(client, track);

  Pilot.drive(client, ourCar, track);

  new Race(client, ourCar, track);
});

client.on('createRace', function (payload) {
  var data = payload.data;

  client.joinRace(botName, botKey, data.trackName, data.carCount);
});

client.on('raw', function (payload) {
  fs.appendFile(outputFile, JSON.stringify(payload) + "\n");
  if (payload.msgType !== 'carPositions') {
    console.log('<', JSON.stringify(payload));

    client.send({
      msgType: "ping",
      data: {}
    });
  }
});

var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);

rl.on('close',function(){
  setTimeout(function () { process.exit(0); }, 0);
});

process.on('SIGINT', function () {
  console.log('SIGINT');
  process.exit(0);
});
process.on('SIGTERM', function () {
  console.log('SIGTERM');
  process.exit(0);
});

