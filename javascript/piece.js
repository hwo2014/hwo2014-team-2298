var _ = require('lodash');

function Piece(piece) {
  this.pieceLength = piece.length;
  this.pieceRadius = piece.radius || Infinity;
  this.pieceAngle = piece.angle || 0;
  this.pieceCanSwitchLanes = piece.switch;
};

Piece.prototype.length = function (lane) {
  var thisLength = this.pieceLength;
  if (!thisLength) {
    thisLength = (2 * Math.PI * Math.abs(this.pieceRadius - lane.distanceFromCenter) * (this.pieceAngle / 360));
  }

  return Math.abs(thisLength);
};

Piece.prototype.absRadius = function (lane) {
  return Math.abs(this.pieceRadius - lane.distanceFromCenter || Infinity);
};

Piece.prototype.canSwitchLanes = function () {
  return this.pieceCanSwitchLanes;
};

module.exports = Piece;
