var _ = require('lodash');
var Car = require('./car.js');

function Race(client, ourCar, track) {
  this.track = track;
  this.ourCar = ourCar;

  client.on('carPositions', function (payload) {
    this.log();
  }.bind(this));

  // Change lanes
  /*
  client.on('carPositions', function (payload) {
    if (this.ourCar) {
      if (this.ourCar.currentPosition.piecePosition && this.ourCar.currentPosition.piecePosition.index === 2) {
         client.send({ msgType: 'switchLane', data: 'Right' });
      }
      if (this.ourCar.currentPosition.piecePosition && this.ourCar.currentPosition.piecePosition.index === 5) {
         client.send({ msgType: 'switchLane', data: 'Left' });
      }
    }
  }.bind(this));
  /**/

  client.on('crash', function (payload) {
    /*
      {"msgType": "crash", "data": {
        "name": "Rosberg",
        "color": "blue"
      }, "gameId": "OIUHGERJWEOI", "gameTick": 3}
    */
    console.log('Car crashed:', payload);
  }.bind(this));

  client.on('spawn', function (payload) {
    /*
      {"msgType": "crash", "data": {
        "name": "Rosberg",
        "color": "blue"
      }, "gameId": "OIUHGERJWEOI", "gameTick": 3}
    */
    console.log('Car restored:', payload);
  }.bind(this));
}

Race.prototype.log = function () {
  console.log('Status: > ',
    'Lap:',
    this.ourCar.currentPosition().lap,
    'Piece:',
    this.ourCar.currentPosition().index + '/' + this.track.pieceCount(),
    'Lane:',
    this.ourCar.currentPosition().laneStart,
    'Speed:',
    this.ourCar.instantSpeed(),
    'Angle:',
    this.ourCar.currentPosition().angle,
    'Throttle:',
    this.ourCar.currentPosition().throttle,
    'At:',
    this.ourCar.currentPosition().distance
  );
};

module.exports = Race;
