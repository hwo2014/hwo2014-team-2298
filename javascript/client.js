var net = require('net');
var JSONStream = require('JSONStream');
var Emitter = require('events').EventEmitter;

module.exports = (function () {
  var knownTracks = [
    'keimola',
    'germany'
  ];
  return {
    knownTracks: knownTracks,
    connect: function (serverPort, serverHost, botKey, botName, track, carCount, password) {
      function send(json, log) {
        netClient.write(JSON.stringify(json));
        return netClient.write('\n');
      }
      function sendJoin(botName, botKey) {
        return send({
          msgType: "join",
          data: {
            name: botName,
            key: botKey
          }
        });
      }
      function sendCreateRace(botName, botKey, track, carCount) {
        return send({
          msgType: "createRace",
          data: {
            botId: {
              name: botName,
              key: botKey
            },
            trackName: track,
            password: password,
            carCount: carCount
          }
        });
      }
      function sendJoinRace(botName, botKey, track, carCount) {
        return send({
          msgType: "joinRace",
          data: {
            botId: {
              name: botName,
              key: botKey
            },
            trackName: track,
            password: password,
            carCount: carCount
          }
        });
      }

      var netClient = net.connect(serverPort, serverHost, function() {
        if (track) {
          return sendJoinRace(botName, botKey, track, carCount);
        } else {
          return sendJoin(botName, botKey);
        }
      });

      var jsonStream = netClient.pipe(JSONStream.parse());
      var emitter = new Emitter();

      jsonStream.on('data', function(data) {
        emitter.emit('raw', data);
        emitter.emit(data.msgType, data);
      });

      jsonStream.on('error', function() {
        emitter.emit('error');
      });
      netClient.on('close', function() {
        emitter.emit('close');
      });

      return {
        send: send,
        joinRace: sendJoinRace,
        on: emitter.on.bind(emitter)
      };
    }
  };
}());
