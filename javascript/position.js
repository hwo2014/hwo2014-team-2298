function Position(data, tick, track) {
  if (!data) {
    this.angle = 0;
    this.index = 0;
    this.distance = 0;
    this.laneStart = 0;
    this.laneEnd = 0;
    this.lap = 0;
    this.tick = 0;
    this.pieceLength = 0;
  } else {
    this.angle = data.angle || 0;
    this.index = data.piecePosition.pieceIndex || 0;
    this.distance = data.piecePosition.inPieceDistance || 0;
    this.laneStart = data.piecePosition.startLaneIndex || 0;
    this.laneEnd = data.piecePosition.endLaneIndex || 0;
    this.lane = data.piecePosition.endLaneIndex || 0;
    this.lap = data.piecePosition.lap || 0;
    this.tick = tick || 0;
    this.pieceLength = track && track.pieceByIndex(this.index).length();
  }
}

Position.prototype.absAngle = function () {
  return Math.abs(this.angle);
};

module.exports = Position;
