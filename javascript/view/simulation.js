function calculatePiecesPosition(track) {
  var startAngle = (track.startingPoint.angle - 90.0) / 180.0 * Math.PI;
  var startingLineAngle = startAngle + Math.PI /2;

  track.lanes.forEach(function(lane, laneIndex) {
    var trackAngle = startAngle;
    var x = track.startingPoint.position.x + lane.distanceFromCenter * Math.cos(startingLineAngle);
    var y = track.startingPoint.position.y + lane.distanceFromCenter * Math.sin(startingLineAngle);

    track.pieces.forEach(function(piece, pieceIndex) {
      var start = {"x": x, "y": y, "angle": trackAngle};
      var length;
      var arc;
      var center, startAngle, endAngle, radius;

      if (piece.length) { // Straight
        x += piece.length * Math.cos(trackAngle);
        y += piece.length * Math.sin(trackAngle);
        length = piece.length;
      } else { // Bend
        if (piece.angle > 0) {
          radius = piece.radius - lane.distanceFromCenter;
          center = {"x": x - (Math.sin(trackAngle) * radius),
                    "y": y + (Math.cos(trackAngle) * radius)};
          startAngle = trackAngle - 0.5 * Math.PI;
        } else {
          radius = piece.radius + lane.distanceFromCenter;
          center = {"x": x + (Math.sin(trackAngle) * radius),
                    "y": y - (Math.cos(trackAngle) * radius)};
          startAngle = trackAngle - 1.5 * Math.PI;
        }
        length = Math.abs(piece.angle) / 180.0 * Math.PI * radius;
        endAngle = startAngle + (piece.angle / 180.0 * Math.PI);
        arc = {"center": center,
               "radius": radius,
               "startAngle": startAngle,
               "endAngle": endAngle,
               "clockwise": piece.angle < 0};
        x = center.x + radius * Math.cos(endAngle);
        y = center.y + radius * Math.sin(endAngle);
        trackAngle += piece.angle / 180.0 * Math.PI;
      }

      if (!piece.lanes) {
        piece.lanes = [];
      }
      piece.lanes.push({"start": start,
                        "arc": arc,
                        "length": length,
                        "end": {"x": x, "y": y, "angle": trackAngle}});
    });
  });

  // Fix start/end alignment
  var firstPiece = track.pieces[0];
  var lastPiece = track.pieces[track.pieces.length - 1];
  for (var i = 0; i < track.lanes.length; i++) {
    lastPiece.lanes[i].end = firstPiece.lanes[i].start;
  }
}

function absolutePosition(track, msgdata) {
  var piecePosition = msgdata.piecePosition;
  var piece = track.pieces[piecePosition.pieceIndex];
  var lane = piece.lanes[piecePosition.lane.startLaneIndex];
  var d = msgdata.piecePosition.inPieceDistance;
  var angle;

  if (!lane.arc) {
    pos = {
      "x": lane.start.x + d * (lane.end.x - lane.start.x) / piece.length,
      "y": lane.start.y + d * (lane.end.y - lane.start.y) / piece.length,
      "yaw": lane.start.angle
    };
  } else {
    var theta = lane.arc.startAngle + (d / lane.length) * (lane.arc.endAngle - lane.arc.startAngle);
    pos = {
      "x": lane.arc.center.x + lane.arc.radius * Math.cos(theta),
      "y": lane.arc.center.y + lane.arc.radius * Math.sin(theta),
      "yaw": lane.start.angle + (d/lane.length) * piece.angle / 180.0 * Math.PI,
      "radius": lane.arc.radius,
      "center": lane.arc.center,
      "clockwise": piece.angle > 0
    };
  }
  pos["angle"] = pos["yaw"] + msgdata.angle / 180.0 * Math.PI;

  return pos;
}

function centerPos(pos, dimensions) {
  var centerDist = (dimensions.guideFlagPosition - dimensions.length / 2);
  return {
    "x": pos.x + centerDist * Math.cos(pos.angle),
    "y": pos.y + centerDist * Math.sin(pos.angle)
  };
}

function rearPos(pos, dimensions) {
  var rearDist = dimensions.length - dimensions.guideFlagPosition;
  return {
    "x": pos.x - rearDist * Math.cos(pos.angle),
    "y": pos.y - rearDist * Math.sin(pos.angle)
  };
}

function vAbs(v) {
  return Math.sqrt(v.x * v.x + v.y * v.y);
}

function dotProduct(v1, v2) {
  return v1.x * v2.x + v1.y * v2.y;
}

function scale(v, s) {
  return {"x": v.x * s, "y": v.y * s};
}

function vSum(v1, v2) {
  return {
    "x": v1.x + v2.x,
    "y": v1.y + v2.y
  };
}

function updateForces(car) {
  if (car.lPos) {
    car.vel = { x: car.pos.x - car.lPos.x, y: car.pos.y - car.lPos.y };
    car.angVel = car.pos.angle - car.lPos.angle;
  } else {
    car.vel = {x: 0, y: 0};
    car.angVel = 0;
  }
  if (car.lVel) {
    car.acc = {x: car.vel.x - car.lVel.x, y: car.vel.y - car.lVel.y };
    car.angAcc = car.angVel - car.lAngVel;
    car.accX = scale(car.vel, dotProduct(car.acc, car.vel) / dotProduct(car.vel, car.vel));
    var center = {
      "x": Math.cos(car.pos.yaw + Math.PI / 2),
      "y": Math.sin(car.pos.yaw + Math.PI / 2)
    };
    car.accC = scale(center, dotProduct(car.acc, center) / dotProduct(center, center));
  } else {
    car.acc = {x: 0, y: 0};
    car.accX = car.acc;
    car.accC = car.acc;
    car.angAcc = 0;
  }

  car.lVel = car.vel;
  car.lPos = car.pos;
  car.lAngVel = car.angVel;
}

function previewForces(car, throttle) {
  var absVel = vAbs(car.vel);

  var Cdrag = 0.00;
  var Crr = 0.1;
  var motor = 1.0;
  var mass = 5.0;
  var dt = 1.0;

  var Ftraction = throttle * motor;
  var Fdrag = Cdrag * Math.pow(absVel, 2);
  var Frr = Crr * absVel;

  var Fx = Ftraction - Fdrag - Frr;
  absAccX = Fx / mass;
  absVel += absAccX / dt;

  var dir = {
    "x": Math.cos(car.pos.yaw),
    "y": Math.sin(car.pos.yaw),
  };
  car.vel = scale(dir, absVel);
  console.log(car.vel);
  car.accX = scale(dir, absAccX);

  if (car.pos.radius) {
    car.angVel = absVel / car.pos.radius;
    var center = {
      "x": Math.cos(pos.yaw + Math.PI / 2),
      "y": Math.sin(pos.yaw + Math.PI / 2)
    };
    car.accC = scale(center, car.angVel * car.angVel * car.pos.radius / vAbs(center));
    if (!car.pos.clockwise) {
      car.angVel = - car.angVel;
      car.accC = scale(car.accC, -1);
    }
  } else {
    car.accC = {"x": 0, "y": 0};
    car.angVel = 0;
  }
  if (car.lAngVel) {
    car.angAcc = car.angVel - car.lAngVel;
  } else {
    car.angAcc = 0;
    car.accC = 0;
  }
  car.acc = vSum(car.accX, car.accC);
  car.lVel = car.vel;
  car.lPos = car.pos;
  car.lAngVel = car.angVel;
}

function previewNextPosition(car, msgdata) {
  var newposition = {
    "id": msgdata.id,
    "angle": msgdata.angle,
    "piecePosition": {
      "pieceIndex": msgdata.piecePosition.pieceIndex,
      "inPieceDistance": msgdata.piecePosition.inPieceDistance,
      "lane": {
        "startLaneIndex": msgdata.piecePosition.lane.startLaneIndex,
        "endLaneIndex": msgdata.piecePosition.lane.endLaneIndex
      },
      "lap": msgdata.lap
    }
  };
  newposition.piecePosition.inPieceDistance += vAbs(car.vel);
  var piece = track.pieces[newposition.piecePosition.pieceIndex];
  var lane = piece.lanes[newposition.piecePosition.lane.startLaneIndex];
  if (newposition.piecePosition.inPieceDistance > lane.length) {
    newposition.piecePosition.pieceIndex += 1;
    if (newposition.piecePosition.pieceIndex == track.pieces.length) {
      newposition.piecePosition.pieceIndex = 0;
      newposition.lap += 1;
    }
    newposition.piecePosition.inPieceDistance -= lane.length;
    newposition.piecePosition.lane.startLaneIndex = newposition.piecePosition.lane.endLaneIndex;
  }
  return newposition;
}
