function transform(ctx) {
  ctx.setTransform(0.5,0,0,0.5,0,0);
  ctx.translate(ctx.canvas.width*1.2, ctx.canvas.height/2);
}

function plotTrack(track, ctx) {
  // Plot Starting line
  ctx.lineWidth = 20;
  var startAngle = (track.startingPoint.angle - 90) / 180.0 * Math.PI;
  var startingLineAngle = startAngle + Math.PI /2;
  var startLineWidth = 50;
  ctx.beginPath();
  ctx.moveTo(
    track.startingPoint.position.x - startLineWidth / 2 * Math.cos(startingLineAngle),
    track.startingPoint.position.y - startLineWidth / 2 * Math.sin(startingLineAngle)
  );
  ctx.strokeStyle = '#FFFF00';
  ctx.lineTo(
    track.startingPoint.position.x + startLineWidth / 2 * Math.cos(startingLineAngle),
    track.startingPoint.position.y + startLineWidth / 2 * Math.sin(startingLineAngle)
  );
  ctx.stroke();

  // Plot track
  ctx.strokeStyle = '#000000';
  ctx.lineWidth = 1;
  track.pieces.forEach(function(piece, pieceIndex) {
    piece.lanes.forEach(function(lane, laneIndex) {
      ctx.beginPath();
      ctx.moveTo(lane.start.x,lane.start.y);
      if (!lane.arc) {
        ctx.lineTo(lane.end.x,lane.end.y);
      } else {
        ctx.arc(lane.arc.center.x,
                lane.arc.center.y,
                lane.arc.radius,
                lane.arc.startAngle,
                lane.arc.endAngle,
                lane.arc.clockwise);
      }
      ctx.stroke();
    });
  });
}

function clearCar(ctx, pos) {
  ctx.clearRect(pos.x-200, pos.y-200, 400, 400);
}

function plotCar(ctx, pos, dimensions) {
  ctx.beginPath();
  var rear = rearPos(pos, dimensions);

  ctx.moveTo(
    rear.x - dimensions.width/2.0 * Math.cos(pos.angle + Math.PI/2),
    rear.y - dimensions.width/2.0 * Math.sin(pos.angle + Math.PI/2)
  );
  ctx.lineTo(
    rear.x + dimensions.width/2.0 * Math.cos(pos.angle + Math.PI/2),
    rear.y + dimensions.width/2.0 * Math.sin(pos.angle + Math.PI/2)
  );
  ctx.strokeStyle = '#ff0000';
  ctx.fillStyle = '#ff0000';
  ctx.lineTo(
    pos.x + dimensions.guideFlagPosition * Math.cos(pos.angle),
    pos.y + dimensions.guideFlagPosition * Math.sin(pos.angle)
  );
  ctx.fill();
  ctx.stroke();
}

function plotAcc(ctx, center, acc) {
  ctx.beginPath();
  ctx.lineWidth = 5;
  ctx.strokeStyle = '#0000ff';
  ctx.moveTo(center.x, center.y);
  ctx.lineTo(
    center.x + 300 * acc.x,
    center.y + 300 * acc.y
  );
  ctx.fill();
  ctx.stroke();
}

function plotVel(ctx, center, vel) {
  ctx.beginPath();
  ctx.lineWidth = 5;
  ctx.strokeStyle = '#008800';
  ctx.moveTo(center.x, center.y);
  ctx.lineTo(
    center.x + 10 * vel.x,
    center.y + 10 * vel.y
  );
  ctx.fill();
  ctx.stroke();
}

function updateFrame(carCtx, car, domSuffix) {
  if (car.lPos) {
    clearCar(carCtx, car.lPos);
  }
  plotCar(carCtx, car.pos, car.dimensions);
  plotVel(carCtx, centerPos(car.pos, car.dimensions), car.vel);
  plotAcc(carCtx, centerPos(car.pos, car.dimensions), car.accX);
  plotAcc(carCtx, centerPos(car.pos, car.dimensions), car.accC);

  document.getElementById("acc_" + domSuffix).innerHTML = vAbs(car.acc).toFixed(3);
  document.getElementById("accC_" + domSuffix).innerHTML = vAbs(car.accC).toFixed(3);
  document.getElementById("accX_" + domSuffix).innerHTML = vAbs(car.accX).toFixed(3);
  document.getElementById("vel_" + domSuffix).innerHTML = vAbs(car.vel).toFixed(3);
  document.getElementById("omega_" + domSuffix).innerHTML = car.angVel.toFixed(3);
  document.getElementById("alfa_" + domSuffix).innerHTML = car.angAcc.toFixed(3);
  document.getElementById("radius_" + domSuffix).innerHTML = car.pos.radius;
}
