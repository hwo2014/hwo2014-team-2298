var _ = require('lodash');

module.exports = (function () {
  return {
    drive: function (client, car, track) {
      var turboAvailableUntil = 0;
      var turboFactor = 1;

      client.on('turboAvailable', function (payload) {
        turboAvailable = car.currentPosition().tick + payload.data.turboDurationTicks;
        turboFactor = payload.data.turboFactor;
      });

      client.on('carPositions', function (payload) {
        var pieceIndex = car.currentPosition().index;
        var nextPiece = track.pieceByIndex(pieceIndex + 1);
        var lane = track.lanes[car.currentPosition().lane];
        var angleSpeed = Math.abs(
          car.currentPosition().angle - car.previousPosition().angle
        );
        var instantSpeed = Math.max(0, car.instantSpeed()); //avoid speed bug between pieces

        if (angleSpeed > 2.5) {
          car.throttle(1 - (0.2 * instantSpeed));
        } else if (nextPiece.absRadius(lane) < 150) {
          if (car.currentPosition().absAngle() > 30) {
            car.throttle(
              Math.pow(0.98, car.currentPosition().absAngle())
            );
          } else {
            car.throttle(
              1 - (
                Math.pow(instantSpeed / 10, 2) *
                Math.pow(100 / nextPiece.absRadius(lane), 0.5)
              )
            );
          }
        } else {
          car.throttle(1);
        }
      });
    }
  };
}());
