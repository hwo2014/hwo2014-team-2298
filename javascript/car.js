var _ = require('lodash');
var Position = require('./position.js');

function Car(name, color) {
  this.name = name;
  this.color = color;
  this.positions = [new Position()];
  this.currentThrottle = 0;
};

Car.prototype.moveOnTrack = function (client, track) {
  this.client = client;

  client.on('carPositions', function (payload) {
    var data = payload.data;

    var ourCarPosition = _.filter(data, function(x) {
      return x.id.name == this.name;
    }, this)[0];

    ourCarPosition && this.setPosition(ourCarPosition, payload.gameTick, track);
  }.bind(this));
};

Car.prototype.setPosition = function (data, tick, track) {
  this.positions.push(new Position(data, tick, track));
};

Car.prototype.currentPosition = function () {
  return _.last(this.positions);
};

Car.prototype.previousPosition = function () {
  return this.positions.slice(-2)[0];
};

Car.prototype.instantSpeed = function (track) {
  if (this.positions.length < 2) {
    return 0;
  }

  var previousPos = this.previousPosition();
  var currentPos = this.currentPosition();
  var distance = currentPos.distance - previousPos.distance;

  if (previousPos.index !== currentPos.index) {
    distance = distance + previousPos.pieceLength;
  }

  var ticksElapsed = currentPos.tick - previousPos.tick;

  return (distance / ticksElapsed) || 0;
};

Car.prototype.throttle = function (amount) {
  amount = Math.min(1, Math.max(0, amount));
  this.client.send({ msgType: "throttle", data: amount });
  this.currentPosition().throttle = amount;
  this.currentThrottle = amount;
}

Car.prototype.increaseThrottle = function (percentage) {
  var to = this.currentThrottle * (1 + percentage);
  if (to > 1) {
    to = 1;
  }
  this.throttle(to);
}

Car.prototype.decreaseThrottle = function (percentage) {
  var to = this.currentThrottle * (1 - percentage);
  if (to < 0) {
    to = 0;
  }
  this.throttle(to);
}

module.exports = Car;
