.PHONY: build run

build:
	./build

run: build
	./run testserver.helloworldopen.com 8091
